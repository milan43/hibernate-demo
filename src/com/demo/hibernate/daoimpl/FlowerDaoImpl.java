package com.demo.hibernate.daoimpl;

import com.demo.hibernate.dao.FlowerDao;
import com.demo.hibernate.model.Flower;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.List;

/*

 @Author melone
 @Date 7/2/18 
 
 */
@Repository("flowerDao")
public class FlowerDaoImpl implements FlowerDao {
    @Autowired
    SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Session currentSession() {
        return sessionFactory.openSession();
    }

    @Override
    public void insertFlower(Flower flower) {
        try(Session session = currentSession()){
            session.getTransaction().begin();
            session.save(flower);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteFlower(int id) {
        try(Session session = currentSession()){
            session.getTransaction().begin();
           // session.delete(session.get(Flower.class,id));
            session.createQuery("delete from Flower where f_id=:id").setParameter("id", id).executeUpdate();
            session.getTransaction().commit();
        }
    }

    @Override
    public Flower getFlowerById(int id) {
        try(Session session = currentSession()) {
        //session.createQuery("From Student s where s.id=:id").setParameter("id", id).uniqueResult();
            return currentSession().get(Flower.class, id);
        }
    }

    @Override
    public List<Flower> getAllFlowers() {
        try(Session session= currentSession()) {
            return sessionFactory.openSession().createQuery("from Flower f").list();
        }
    }

    @Override
    public void updateFlower(int id) {
       try(Session session = currentSession()){
           session.getTransaction().begin();
           session.createQuery("update Flower set f_name=:name where f_id =:id")
                   .setParameter("name", "Lilly").setParameter("id", id).executeUpdate();
           session.getTransaction().commit();
       }
    }
}
