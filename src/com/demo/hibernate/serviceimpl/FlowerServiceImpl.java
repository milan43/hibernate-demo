package com.demo.hibernate.serviceimpl;

import com.demo.hibernate.dao.FlowerDao;
import com.demo.hibernate.model.Flower;
import com.demo.hibernate.service.FlowerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/*

 @Author melone
 @Date 7/2/18 
 
 */
@Service("flowerService")//bean name is created as class name if value is not provided
public class FlowerServiceImpl implements FlowerService {
    @Autowired
    private FlowerDao flowerDao;

    public void setFlowerDao(FlowerDao flowerDao) {
        this.flowerDao = flowerDao;
    }

    @Override
    public void insertFlower(Flower flower) {
        flowerDao.insertFlower(flower);
    }

    @Override
    public void deleteFlower(int id) {
        flowerDao.deleteFlower(id);
    }

    @Override
    public Flower getFlowerById(int id) {
        return flowerDao.getFlowerById(id);
    }

    @Override
    public List<Flower> getAllFlowers() {
        return flowerDao.getAllFlowers();
    }

    @Override
    public void updateFlower(int id) {
        flowerDao.updateFlower(id);
    }
}
