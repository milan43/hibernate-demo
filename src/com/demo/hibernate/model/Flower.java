package com.demo.hibernate.model;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;

/*

 @Author melone
 @Date 7/2/18 
 
 */

@Entity
//if table is not mentioned class name is used as table name
@DynamicUpdate//update only the changed column
public class Flower {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "f_id")
    private int id;
    @Column(name = "f_name")
    private String name;
    @Column(name = "f_color")//if column nane is not mentioned then variable name is used in table
    private String color;
    @Column(name = "f_insertdate")
    @Temporal(TemporalType.DATE)//to only insert date without time
    private Date insertDate;
    @Transient//ignores field to be inserted in database table
    private int noField;

    public void setId(int id) {
        this.id = id;
    }

    public Flower(){
    }
    public void setName(String name) {
        this.name = name;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public void setNoField(int noField) {
        this.noField = noField;
    }

    public Flower(String name, String color, Date insertDate) {
        this.name = name;
        this.color = color;
        this.insertDate = insertDate;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", color='" + color + '\'' +
                ", insertDate=" + insertDate +
                '}';
    }
}
