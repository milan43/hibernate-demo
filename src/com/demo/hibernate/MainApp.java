package com.demo.hibernate;

import com.demo.hibernate.model.Flower;
import com.demo.hibernate.service.FlowerService;
import com.demo.hibernate.serviceimpl.FlowerServiceImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Date;

/*

 @Author melone
 @Date 7/2/18 
 
 */
public class MainApp {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("com/demo/hibernate/config/hibernate-config.xml");
        FlowerService service = context.getBean("flowerService", FlowerServiceImpl.class);
        //service.insertFlower(new Flower("Rose", "Black", new Date()));
        //System.out.println(service.getFlowerById(1));
/*        service.updateFlower(1);
        System.out.println(service.getAllFlowers());
        service.deleteFlower(1);*/
        System.out.println(service.getAllFlowers());
    }
}
