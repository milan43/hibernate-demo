package com.demo.hibernate.dao;

import com.demo.hibernate.model.Flower;

import java.util.List;

/*

 @Author melone
 @Date 7/2/18 
 
 */
public interface FlowerDao {
    void insertFlower(Flower flower);
    void deleteFlower(int id);
    Flower getFlowerById(int id);
    List getAllFlowers();
    void updateFlower(int id);
}
