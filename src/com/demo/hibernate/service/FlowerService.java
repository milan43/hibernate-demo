package com.demo.hibernate.service;

import com.demo.hibernate.model.Flower;

import java.util.List;

/*

 @Author melone
 @Date 7/2/18 
 
 */
public interface FlowerService {
    void insertFlower(Flower flower);
    void deleteFlower(int id);
    Flower getFlowerById(int id);
    List<Flower> getAllFlowers();
    void updateFlower(int id);
}
